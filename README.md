# Aspire's Eslint Config #

Aspire's general config for Eslint.

## Setup ##

1. Add the module to the project's `package.json` via this project's url
1. npm/yarn install
1. Add an `extends` entry into the project's `.eslintrc` pointing to the `index.js` inside of `node_modules`.

#### package.json ####

```
{
  devDependencies: {
    "eslint-config-aspire": "git+ssh://git@bitbucket.org/aspirehealth/eslint-config-aspire.git"
  }
}
```

#### .eslintrc ####

```
{
  "extends": "./node_modules/eslint-config-aspire/index.js"
}
```
